 <?php get_header();?>

 <div class="container">
    <section class="boxes">
      <div class="row">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="box">
            <!-- box's header - name and logo -->
            <div class="box-header">
              <h3><?php the_title();?></h3>
              <img src="<?php echo get_the_post_thumbnail_url(get_the_ID());?>"/>
            </div>
            <!-- box's content - links -->
            <?php $image = get_field('background_image');
            if( !empty($image) ): ?>
              <div class="box-links" style="background-image: url(<?php esc_html_e($image['url']); ?>);">
                <ul>
                <?php  if( have_rows('post_links') ): ?>
                    <?php while ( have_rows('post_links') ) : the_row();  ?>
                      <li><a href="<?php the_sub_field('link_url'); ?>"><?php the_sub_field('link_text'); ?></a></li>
                    <?php endwhile; ?>
                  <?php endif; ?>   
                </ul>
              </div>
            <?php endif; ?>
          </div>
        </div>
      <?php endwhile; endif;?>
    </div>
  </section>
</div>
  
<?php get_footer();?>