<!-- footer -->
<div class="container">
    <footer class="footer" style="background-color:<?php the_field('footer_color', 'option'); ?>">
      <div class="row">
        <div class="col-12">
          <div class="footer-phone">
            <i class="lni-phone-handset"></i>
            <a href="<?php esc_url('tel:'.'+381631064600', 'citadela')?>"><?php the_field('phone_number', 'option'); ?></a>
          </div>
          <p><?php esc_html_e('Sva prava zadržana', 'citadela')?> <b><?php esc_html_e('Citadela d.o.o.', 'citadela')?></b> <?php esc_html_e('2019.', 'citadela')?></p>
          <?php if(is_active_sidebar('footer-sidebar')){
          dynamic_sidebar('footer-sidebar'); }?>
        </div>
      </div>
    </footer>
  </div>

  <?php wp_footer();?>
</body>