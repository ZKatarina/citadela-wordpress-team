<?php get_header();?>

<div class="container">
  <div class="row">
      <div class="col">
        <br/>
        <h3>Ovo je stranica '<?php the_title();?>'</h3><br>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
        <p><?php the_content();?></p>
        <?php endwhile; endif;?>
			</div>
		</div>
</div>

<?php get_footer();?>s