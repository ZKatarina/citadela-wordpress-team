<?php
/*
    Template Name: Home
*/  
get_header();?>

<!-- carousel -->
<section class="banner">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php  if( have_rows('carousel') ):
	        $i = 1; 
	        echo '<div id="carouselExampleSlidesNav2" class="carousel slide" data-ride="carousel">
			    <div class="carousel-inner">';
          while ( have_rows('carousel') ) : the_row();
	          $image = get_sub_field('carousel_image'); ?>
            <div class="carousel-item <?php if($i == 1) echo 'active';?>">
              <img class="d-block w-100" src="<?php esc_html_e($image['url']); ?>">
              <div class="title">
                <h1><?php the_sub_field('carousel_title'); ?></h1>
                <h2><?php the_sub_field('carousel_subtitle'); ?></h2>
              </div>
            </div>
          <?php $i++;
          endwhile;
          echo '</div>
              <a class="carousel-control-prev" href="#carouselExampleSlidesNav2" role="button" data-slide="prev">
              <i class="lni-chevron-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleSlidesNav2" role="button" data-slide="next">
                <i class="lni-chevron-right"></i>
                </a>
            </div>';
          endif; ?>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <section class="boxes">
    <div class="row">
    <?php $post_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
    <?php if ( $post_query->have_posts() ) : while ( $post_query->have_posts() ) : $post_query->the_post();?>
      <div class="col-xl-3 col-lg-4 col-sm-6">
        <div class="box">
          <!-- box's header - name and logo -->
          <div class="box-header">
          <a href="<?php the_permalink(); ?>"><h3><?php the_title();?></h3></a>
            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID());?>"/>
          </div>
          <!-- box's content - links -->
          <?php $image = get_field('background_image');
            if( !empty($image) ): ?>
              <div class="box-links" style="background-image: url(<?php esc_html_e($image['url']); ?>);">
                <ul>
                <?php  if( have_rows('post_links') ): ?>
                    <?php while ( have_rows('post_links') ) : the_row();  ?>
                      <li><a href="<?php the_sub_field('link_url'); ?>"><?php the_sub_field('link_text'); ?></a></li>
                    <?php endwhile; ?>
                  <?php endif; ?>   
                </ul>
              </div>
            <?php endif; ?>
        </div>
      </div>
    <?php endwhile; endif;?>
    </div>
  </section>
</div>

<?php get_footer();?>