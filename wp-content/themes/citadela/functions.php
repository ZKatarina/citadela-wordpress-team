<?php

// load scripts
function load_stylesheet(){
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'lineicons', 'https://cdn.lineicons.com/1.0.1/LineIcons.min.css', false);
    wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&display=swap', false );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), false, true );
    wp_enqueue_style( 'stylesheet', get_template_directory_uri() . '/css/main.css' );
}
add_action('wp_enqueue_scripts', 'load_stylesheet');


// add support
if ( ! function_exists( 'citadela_setup' ) ) :
	
	function citadela_setup() {
		load_theme_textdomain( 'citadela', get_template_directory());
        add_theme_support( "title-tag" );
        add_theme_support( "content" );
        add_theme_support( "title-tag" );
        add_theme_support( "widgets" );
        add_theme_support( 'html5', array(
            'comment-form',
            'comment-list'
        ) );
        add_theme_support( 'post-thumbnails' );
        register_nav_menu( 'top-navigation', esc_html__( 'Top Navigation', 'citadela' ) );
    }
endif;
add_action( 'after_setup_theme', 'citadela_setup' );

if ( ! function_exists( 'citadela_widgets_init' ) ) {
    
    function citadela_widgets_init() {

    register_sidebar( array(
        'name' => 'Footer Sidebar',
        'id' => 'footer-sidebar',
        'description' => 'Appears in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) );
    }
}
add_action( 'widgets_init', 'citadela_widgets_init' );

// add theme options to change bg color of header, footer and contact phone
if( function_exists('acf_add_options_page') ) {
	
    acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme_options',
        'capability'	=> 'edit_posts',
        'parent_slug' 	=> '',
        'position' 	    => false,
        'icon-url' 	    => false,
		'redirect'		=> false
    ));
    
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Header',
        'menu_title'	=> 'Header',
        'menu_slug' 	=> 'header',
        'capability'	=> 'edit_posts',
        'parent_slug' 	=> 'theme_options',
        'position' 	    => false,
        'icon-url' 	    => false,
    ));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
        'menu_title'	=> 'Footer',
        'menu_slug' 	=> 'footer',
        'capability'	=> 'edit_posts',
        'parent_slug' 	=> 'theme_options',
        'position' 	    => false,
        'icon-url' 	    => false,
    ));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Contact phone',
        'menu_title'	=> 'Contact phone',
        'menu_slug' 	=> 'contact_phone',
        'capability'	=> 'edit_posts',
        'parent_slug' 	=> 'theme_options',
        'position' 	    => false,
        'icon-url' 	    => false,
	));
	
}

//add local ACF fields
if( function_exists('acf_add_local_field_group') ):
    
    // Header options fields
    acf_add_local_field_group(array(
        'key' => 'group_1',
        'title' => 'Header options',
        'fields' => array (
            array (
                'key' => 'field_11',
                'label' => 'Header color',
                'name' => 'header_color',
                'type' => 'text',
            ),
            array (
                'key' => 'field_12',
                'label' => 'Logo',
                'name' => 'logo',
                'type' => 'image',
                'return_format' => 'array',
                'preview_size' => 'large',
                'library' => 'all',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'header',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
    ));

    // Footer options fields
    acf_add_local_field_group(array(
        'key' => 'group_5',
        'title' => 'Change color',
        'fields' => array (
            array (
                'key' => 'field_52',
                'label' => 'Footer color',
                'name' => 'footer_color',
                'type' => 'text',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'footer',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
    ));

     // Contact phone options fields
     acf_add_local_field_group(array(
        'key' => 'phone_number',
        'title' => 'Phone number',
        'fields' => array (
            array (
                'key' => 'field_41',
                'label' => 'Phone number',
                'name' => 'phone_number',
                'instructions' => 'Enter the phone number which will be displayed in header and footer',
                'type' => 'text',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'contact_phone',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
    ));
    
    // Posts meta fields
    acf_add_local_field_group(array(
        'key' => 'post_fields',
        'title' => 'Post Fields',
        'fields' => array (
            array (
                'key' => 'field_1',
                'label' => 'Background image',
                'name' => 'background_image',
                'type' => 'image',
                'return_format' => 'array',
                'preview_size' => 'large',
                'library' => 'all',
            ),
            array (
            'key' => 'field_2',
            'label' => 'Links',
            'name' => 'post_links',
            'type' => 'repeater',
            'instructions' => 'Enter text and url for the links on this post',
            'layout' => 'table',
            'sub_fields' => array (
                   
                array (
                    'key' => 'field_21',
                    'label' => 'Link text',
                    'name' => 'link_text',
                    'type' => 'text',
                ),
                array (
                    'key' => 'field_22',
                    'label' => 'Link url',
                    'name' => 'link_url',
                    'type' => 'url',
                ),
            ),
        'row_min' => 0,
        'row_limit' => '',
        'layout' => 'table',
        'button_label' => 'Add row',
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'post',
            ),
        ),
    ),
    'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
    ));

    // Carousel fileds
    acf_add_local_field_group(array(
        'key' => 'carousel_fields',
        'title' => 'Carousel Fields',
        'fields' => array (
            array (
            'key' => 'field_3',
            'label' => 'Carousel',
            'name' => 'carousel',
            'type' => 'repeater',
            'instructions' => 'Enter carousel image, title and subtitle',
            'layout' => 'table',
            'sub_fields' => array (
                   
                array (
                    'key' => 'field_31',
                    'label' => 'Carousel image',
                    'name' => 'carousel_image',
                    'type' => 'image',
                    'return_format' => 'array',
                    'preview_size' => 'large',
                    'library' => 'all',
                ),
                array (
                    'key' => 'field_32',
                    'label' => 'Carousel title',
                    'name' => 'carousel_title',
                    'type' => 'text',
                ),
                array (
                    'key' => 'field_33',
                    'label' => 'Carousel subtitle',
                    'name' => 'carousel_subtitle',
                    'type' => 'text',
                ),
            ),
        'row_min' => 0,
        'row_limit' => '',
        'layout' => 'table',
        'button_label' => 'Add row',
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'page_template',
                'operator' => '==',
                'value' => 'home.php',
            ),
        ),
    ),
    'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
    ));
    
endif;

