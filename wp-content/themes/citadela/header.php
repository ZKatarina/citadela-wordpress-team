<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head();?>  
</head>
<body <?php body_class(); ?>>
  <header>
    <div class="container">
      <div class="menu" style="background-color:<?php the_field('header_color', 'option'); ?>">
        <div class="row">
          <div class="col-2">
            <!-- Citadela logo -->
            <?php $logo = get_field( 'logo', 'option' ); ?>
            <div class="logo" style="background-image: url(<?php esc_html_e($logo['url']); ?>);"></div>
          </div>
          <div class="col-10">
            <nav class="navbar navbar-expand-lg navbar-light">
              <button class="navbar-toggler navbar-toggler-left" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="line"></span> 
                <span class="line"></span> 
                <span class="line"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <!-- menu -->
                <?php if ( has_nav_menu( 'top-navigation')) :
                  wp_nav_menu (
                    array ('theme_location' => 'top-navigation',
                           'menu_class' => 'navbar-nav')) ?>
                <?php endif; ?>
                <!-- Contact phone -->
                <div class="phone">
                  <p>
                    <i class="lni-phone-handset"></i>
                    <?php esc_html_e('Pozovite nas', 'citadela')?>
                    <a href="<?php esc_url('tel:'.'+381631064600', 'citadela')?>"><?php the_field('phone_number', 'option'); ?></a>
                  </p>
                </div>        
              </div>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>