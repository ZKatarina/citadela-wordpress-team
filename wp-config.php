<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'citadela' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3*G]49u&,MP#n-%p%ox#N=6~+yBh_o}`Z=i8I,Fmo?8AnM$_wvN%X7@l68rf<C.M' );
define( 'SECURE_AUTH_KEY',  'dLF*#t1;lOJC;my9:?b3-ZA{o_cM-`+AF_lb9IbQ^q5Y)H/Z e~<tlSsL2_o}oxy' );
define( 'LOGGED_IN_KEY',    'wipM=?J]]p?Wz0Xy:_G<GYdp8&#JSC]+D!Pv}|r@vhX]f=^0?W6!H2M>Z/{?j7P+' );
define( 'NONCE_KEY',        '4I?oTi;5e3%$,Sxy9k5#sh-ZOLNgrwM+rHlntp,XO}]BD`#Gip(|Y-J@#UC+e0sI' );
define( 'AUTH_SALT',        '9@o|:=al&DiGJzWGY# 3-!k+kYo~+CdBEGv cS?_k),(l^Cf$_PQ5$}88O.s6[}7' );
define( 'SECURE_AUTH_SALT', '8#v0Gxz+9vEU&J3_R+ZDOG0uo.*N4F!,cdy<OtXy{zIL09gKCJQ&$%Xb;p,y#wzn' );
define( 'LOGGED_IN_SALT',   'Tt9@^Zb^C[/qzFx;1CtHIenPLM&5OTqPZ(}<L[H6]`}PSAYg~I,7vZ>: XR.YD}9' );
define( 'NONCE_SALT',       'n;:)9[FRD85qtEC:iEK2^ye`[$dwa]F0#X#(N7)}HCg%Fgwa;;1z#I(fPp%iZu{v' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
